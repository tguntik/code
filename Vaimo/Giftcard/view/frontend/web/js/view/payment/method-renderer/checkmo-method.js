/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/* @api */
define([
    'jquery',
    'Magento_Checkout/js/view/payment/default',
], function ($, Component) {
    'use strict';

    var quoteItemData = window.checkoutConfig.quoteItemData;

    return Component.extend({

        defaults: {
            template: 'Vaimo_Giftcard/payment/checkmo'
        },

        quoteItemData: quoteItemData,

        isGiftExist: false,

        /**
         * Returns send check to info.
         *
         * @return {*}
         */
        getMailingAddress: function () {
            return window.checkoutConfig.payment.checkmo.mailingAddress;
        },

        /**
         * Returns payable to info.
         *
         * @return {*}
         */
        getPayableTo: function () {
            var self = this;

            $('#customField1').css("display", "none");
            $('#customField2').css("display", "none");
            $('#customField3').css("display", "none");

            var allProducts = this.quoteItemData;

            $.each( allProducts, function( key, value ) {
                var productType = value.product.type_id;

                if(productType == 'giftcard_product_type') {

                    self.isGiftExist = true;
                    $('#customField1').css("display", "block");
                    $('#customField1').css("max-width", "500px");
                    $('#customField2').css("display", "block");
                    $('#customField2').css("max-width", "500px");
                    $('#customField3').css("display", "block");
                    $('#customField3').css("max-width", "500px");
                }
            });
            return window.checkoutConfig.payment.checkmo.payableTo;
        },

        getData: function() {
            return {
                'method': this.item.method,
                'additional_data': {
                    'receiver_mail': $('#checkmo_receiverMail').val(),
                    'receiver_name': $('#checkmo_receiverName').val(),
                    'delivery_date': $('#checkmo_deliveryDate').val()
                }
            };
        },

        customValidation: function () {
            var receiverMail = $('#checkmo_receiverMail').val();
            var receiverName = $('#checkmo_receiverName').val();
            var deliveryDate = $('#checkmo_deliveryDate').val();

            if (this.isGiftExist == false) {
                return true;
            } else if (receiverMail != "" && receiverName != "" && deliveryDate != "") {
                return true;
            } else {
                $('#myErrorMess1').css("display", "block");
                $('#myErrorMess2').css("display", "block");
                $('#myErrorMess3').css("display", "block");
                return false;
            }
        }
    });
});
