<?php

namespace Vaimo\Giftcard\Model;

use Vaimo\Giftcard\Model\ResourceModel\Giftcard\CollectionFactory;
use Vaimo\Giftcard\Model\GiftcardFactory;
use Magento\Cms\Api\Data\PageInterface;

/**
 * Class GiftcardRepository
 * @package Vaimo\Giftcard\Model
 */
class GiftcardRepository
{
    /**
     * @var CollectionFactory
     */
    protected $collection;
    /**
     * @var \Vaimo\Giftcard\Model\GiftcardFactory
     */
    protected $giftcardFactory;

    /**
     * GiftcardRepository constructor.
     * @param CollectionFactory $collection
     * @param \Vaimo\Giftcard\Model\GiftcardFactory $giftcardFactory
     */
    public function __construct(CollectionFactory $collection, GiftcardFactory $giftcardFactory)
    {
        $this->collection = $collection;
        $this->giftcardFactory = $giftcardFactory;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        $data = $this->collection->create();
        $item = $data->getItems();
        return $item;
    }

    public function getByOrderId($id)
    {
        $data = $this->collection->create();
        $item = $data->getItemsByColumnValue('order_id', $id);
        return $item;
    }
    /**
     * @param $data
     */
    public function save($data)
    {
        $giftcard = $this->giftcardFactory->create();
        $giftcard->setData($data);
        $giftcard->save();
    }

    public function setHoldData($id, $date)
    {
        $data = $this->giftcardFactory->create()->getCollection();
        $giftcardOnHold = $data->getItemsByColumnValue('order_id', $id);
        $giftcardOnHold[0]->setHold($date);
        $giftcardOnHold[0]->save();
    }

    public function deleteHoldData($id)
    {
        $data = $this->giftcardFactory->create()->getCollection();
        $giftcardRemoveHold = $data->getItemsByColumnValue('order_id', $id);

        $giftcardRemoveHold[0]->setHold("");
        $giftcardRemoveHold[0]->save();
    }
}