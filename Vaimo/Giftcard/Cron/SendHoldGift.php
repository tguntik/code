<?php


namespace Vaimo\Giftcard\Cron;

use Vaimo\Giftcard\Helper\Email;
use Vaimo\Giftcard\Model\GiftcardRepository;

class SendHoldGift
{
    /**
     * @var Email
     */
    protected $email;
    /**
     * @var GiftcardRepository
     */
    protected $repository;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $date;

    public function __construct(
        Email $email,
        GiftcardRepository $repository,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $date
    )
    {
        $this->email = $email;
        $this->repository = $repository;
        $this->date =  $date;
    }

    public function sendHoldGift()
    {
        $allItems = $this->repository->getData();
        $currentData = $this->date->date()->format('d-m-Y');

        foreach ($allItems as $item) {
            $dataHold = $item['hold'];

            if ($dataHold == $currentData) {
                $this->email->sendEmail($item['receiver_name'], $item['receiver_mail'], $item['giftcard_code'], 10);
                $this->repository->deleteHoldData($item['order_id']);
            }
        }
    }
}