<?php

namespace Vaimo\Giftcard\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Vaimo\Giftcard\Helper\Email;
use Vaimo\Giftcard\Model\GiftcardRepository;

/**
 * Class SendMail
 * @package Vaimo\Giftcard\Observer
 */
class SendMail implements ObserverInterface
{
    /**
     * @var Email
     */
    protected $email;
    /**
     * @var GiftcardRepository
     */
    protected $repository;

    protected $date;

    public function __construct(
        Email $email,
        GiftcardRepository $repository,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $date
    )
    {
        $this->email = $email;
        $this->repository = $repository;
        $this->date = $date;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $data = $order->getAllItems();

        foreach ($data as $item) {
            $productType = $item->getProductType();

            if ($productType == 'giftcard_product_type') {

                $orderId = $order->getId();
                $giftCardField = $this->repository->getByOrderId($orderId);
                $giftCardData = $giftCardField[0]->getData();
                $name = $giftCardData['receiver_name'];
                $mail = $giftCardData['receiver_mail'];
                $code = $giftCardData['giftcard_code'];
                $deliveryDate = $giftCardData['delivery_date'];
                $price = $item->getPrice();

                $currentData = $this->date->date()->format('d-m-Y');

                if ($currentData == $deliveryDate || $deliveryDate == "") {
                    $this->email->sendEmail($name, $mail, $code, $price);
                } else {
                    $this->repository->setHoldData($orderId, $deliveryDate);
                }

            }
        }
    }
}