<?php

namespace Vaimo\Giftcard\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

use Vaimo\Giftcard\Helper\GenerateGiftCode;
use Vaimo\Giftcard\Model\GiftcardRepository;

/**
 * Class SaveAdditionalCheckmoDataObserver
 * @package Vaimo\Giftcard\Observer
 */
class SaveAdditionalCheckmoDataObserver implements ObserverInterface
{
    /**
     * @var \Magento\Webapi\Controller\Rest\InputParamsResolver
     */
    protected $_inputParamsResolver;
    /**
     * @var \Magento\Quote\Model\QuoteRepository
     */
    protected $_quoteRepository;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    /**
     * @var \Magento\Framework\App\State
     */
    protected $_state;
    /**
     * @var GenerateGiftCode
     */
    protected $generateGiftCode;
    /**
     * @var GiftcardRepository
     */
    protected $giftRepository;

    public function __construct(
        \Magento\Webapi\Controller\Rest\InputParamsResolver $inputParamsResolver,
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\State $state,
        GenerateGiftCode $generateGiftCode,
        GiftcardRepository $giftRepository
    )
    {
        $this->_inputParamsResolver = $inputParamsResolver;
        $this->_quoteRepository = $quoteRepository;
        $this->logger = $logger;
        $this->_state = $state;
        $this->generateGiftCode = $generateGiftCode;
        $this->giftRepository = $giftRepository;
    }

    public function execute(EventObserver $observer)
    {

        $inputParams = $this->_inputParamsResolver->resolve();

//        if($this->_state->getAreaCode() != \Magento\Framework\App\Area::AREA_ADMINHTML){

        foreach ($inputParams as $inputParam) {
            if ($inputParam instanceof \Magento\Quote\Model\Quote\Payment) {

//                    save order id, receiver_mail and gift code to custom table
                $receiver_data = $inputParam->getData('additional_data');
                $mail = filter_var($receiver_data['receiver_mail'], FILTER_SANITIZE_STRING);
                $name = filter_var($receiver_data['receiver_name'], FILTER_SANITIZE_STRING);
                $deliveryDate = filter_var($receiver_data['delivery_date'], FILTER_SANITIZE_STRING);

                if ($mail != "") {
                    $paymentOrder = $observer->getEvent()->getPayment();
                    $order = $paymentOrder->getOrder();
                    $order_id = $order->getId();

                    $price = null;
                    $items = $order->getAllItems();

                    foreach ($items as $item) {
                        $productType = $item->getProductType();
                        if ($productType == 'giftcard_product_type') {
                            $price = $item->getPrice();
                        }
                    }

                    $code = $this->generateGiftCode->generateGiftCode($price);
                    $data = ['order_id' => $order_id, 'receiver_mail' => $mail, 'receiver_name' => $name, 'giftcard_code' => $code, 'delivery_date' => $deliveryDate];

                    $this->giftRepository->save($data);
                }
            }
        }
//        }
    }
}