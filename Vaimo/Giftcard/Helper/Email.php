<?php

namespace Vaimo\Giftcard\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Escaper;
use Magento\Framework\Mail\Template\TransportBuilder;
use Vaimo\Giftcard\Helper\GetSenderEmailData;

/**
 * Class Email
 * @package Vaimo\Giftcard\Helper
 */
class Email extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var StateInterface
     */
    protected $inlineTranslation;
    /**
     * @var Escaper
     */
    protected $escaper;
    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    /**
     * @var \Vaimo\Giftcard\Helper\GetSenderEmailData
     */
    protected $getSenderEmail;
    /**
     * Email constructor.
     * @param Context $context
     * @param StateInterface $inlineTranslation
     * @param Escaper $escaper
     * @param TransportBuilder $transportBuilder
     */
    public function __construct(
        Context $context,
        StateInterface $inlineTranslation,
        Escaper $escaper,
        TransportBuilder $transportBuilder,
        GetSenderEmailData $getSenderEmail
    )
    {
        parent::__construct($context);
        $this->inlineTranslation = $inlineTranslation;
        $this->escaper = $escaper;
        $this->transportBuilder = $transportBuilder;
        $this->logger = $context->getLogger();
        $this->getSenderEmail = $getSenderEmail;
    }

    /**
     * @param $name
     * @param $mail
     * @param $code
     * @param $price
     */
    public function sendEmail($name, $mail, $code, $price)
    {
        $recieverName = $name;
        $cardCode = $code;
        $giftPrice = $price;

        $sender = $this->getSenderEmail->getGeneralConfig('sender-mail');
        try {
            $this->inlineTranslation->suspend();
            $sender = [
                'name' => $this->escaper->escapeHtml('Test'),
                'email' => $this->escaper->escapeHtml($sender),
            ];

            $transport = $this->transportBuilder->setTemplateIdentifier('email_to_send_giftcode')->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                ]
            )
                ->setTemplateVars([
                    'name' => $recieverName,
                    'cardCode' => $cardCode,
                    'price' => $giftPrice,
                ])
                ->setFrom($sender)
                ->addTo($mail)
                ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
        }
    }
}