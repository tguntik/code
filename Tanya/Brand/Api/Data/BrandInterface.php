<?php

namespace Tanya\Brand\Api\Data;

/**
 * Interface BrandInterface
 * @package Tanya\Brand\Api\Data
 * @api
 */
interface BrandInterface
{
    /**
     * @return int
     */
    public function getId();
    /**
     * @return string
     */
    public function getName();
//    /**
//     * @return string
//     */
//    public function getLogo();
    /**
     * @return string
     */
    public function getDescription();
    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);
    /**
     * @param string $name
     * @return $this
     */
    public function setName($name);
//    /**
//     * @param string $logo
//     * @return $this
//     */
//    public function setLogo($logo);
    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description);

}