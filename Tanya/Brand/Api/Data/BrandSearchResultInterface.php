<?php

namespace Tanya\Brand\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;
/**
 * Interface BrandSearchResultInterface
 * @package Tanya\Brand\Api\Data
 */
interface BrandSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return \Tanya\Brand\Api\Data\BrandInterface[]
     */
    public function getItems();
    /**
     * @param array $items
     * @return \Tanya\Brand\Api\Data\BrandSearchResultInterface
     */
    public function setItems(array $items);
}