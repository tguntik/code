<?php

namespace Tanya\Brand\Api;

use Tanya\Brand\Api\Data\BrandInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface BrandRepositoryInterface
 * @package Tanya\Brand\Api
 * @api
 */
interface BrandRepositoryInterface
{
    /**
     * Save brand
     *
     * @param Data\BrandInterface $brand
     * @return \Tanya\Brand\Api\Data\BrandInterface
     */
    public function save(Data\BrandInterface $brand);
    /**
     * Get brand by id
     *
     * @param $brandId
     * @return mixed
     */
    public function getById($brandId);
    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Tanya\Brand\Api\Data\BrandSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
    /**
     * Delete brand
     *
     * @param Data\BrandInterface $brand
     * @return mixed
     */
    public function delete(Data\BrandInterface $brand);

    /**
     * @param BrandInterface $brand
     * @return mixed
     */
    public function deleteById(Data\BrandInterface $brand);


    /**
     * @param $brandName
     * @return mixed
     */
    public function getInfoByBrandName($brandName);
}