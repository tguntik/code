<?php

namespace Tanya\Brand\Model;

use Magento\Framework\Exception\StateException;
use Tanya\Brand\Api\BrandRepositoryInterface;
use Tanya\Brand\Api\Data\BrandInterface;
use Tanya\Brand\Model\Resource\Brand as BrandResource;
use Tanya\Brand\Api\Data;
use Tanya\Brand\Api\Data\BrandSearchResultInterface;
use Tanya\Brand\Api\Data\BrandSearchResultInterfaceFactory;

use Tanya\Brand\Model\BrandModelFactory;

use Magento\Framework\Api\SearchCriteriaInterface;
use Tanya\Brand\Model\Resource\Brand;
use Tanya\Brand\Model\Resource\Brand\CollectionFactory as BrandCollectionFactory;

class BrandRepository implements BrandRepositoryInterface
{
    /**
     * @var array
     */
    private $registry = [];
    /**
     * @var \Tanya\Brand\Model\BrandModelFactory
     */
    private $brandFactory;
    /**
     * @var BrandResource
     */
    private $brandResource;
    /**
     * @var BrandCollectionFactory
     */
    private $brandCollectionFactory;
    /**
     * @var BrandSearchResultInterfaceFactory
     */
    private $brandSearchResultFactory;
    /**
     * BrandRepository constructor.
     * @param BrandResource $brandResource
     * @param \Tanya\Brand\Model\BrandModelFactory $brandFactory
     * @param BrandCollectionFactory $brandCollectionFactory
     * @param BrandSearchResultInterfaceFactory $brandSearchResultFactory
     */
    public function __construct(
        BrandResource $brandResource,
        BrandModelFactory $brandFactory,
        BrandCollectionFactory $brandCollectionFactory,
        BrandSearchResultInterfaceFactory $brandSearchResultFactory
    ){
        $this->brandResource = $brandResource;
        $this->brandFactory = $brandFactory;
        $this->brandCollectionFactory = $brandCollectionFactory;
        $this->brandSearchResultFactory = $brandSearchResultFactory;
    }
    /**
     * @param Data\BrandInterface $brand
     * @return BrandInterface
     * @throws StateException
     */
    public function save(Data\BrandInterface $brand)
    {
        try {
            if (empty($brand->getId())) {
                $brand->setId(null);
            }
            /** @var Brand $brand */
            $this->brandResource->save($brand);
        } catch (\Exception $exception) {
            throw new StateException(__('Unable to save brand #%1', $brand->getId()));
        }
        return $brand;
    }
    public function getById($brandId)
    {
        $parameter = $this->brandCollectionFactory->create();
        $brand = $parameter->getItemById($brandId);
        return $brand;
    }
    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return BrandSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->brandCollectionFactory->create();
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        /** @var BrandSearchResultInterface $searchResult */
        $searchResult = $this->brandSearchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());
        return $searchResult;
    }

    /**
     * @param BrandInterface $brand
     * @return bool
     * @throws StateException
     */
    public function delete(Data\BrandInterface $brand)
    {
        try {
            /** @var Brand $brand */
            $this->brandResource->delete($brand);
            unset($this->registry[$brand->getId()]);
        } catch (\Exception $e) {
            throw new StateException(__('Unable to remove post #%1', $brand->getId()));
        }

        return true;
    }

    public function deleteById(Data\BrandInterface $brand)
    {
    }

    public function getInfoByBrandName($brandName)
    {
        // TODO: Implement getInfoByBrandName() method.
    }
}













//use Tanya\Brand\Api\BrandRepositoryInterface;
//use Tanya\Brand\Model\Resource\Brand\CollectionFactory;
//use Magento\Cms\Api\Data\PageInterface;
//use Tanya\Brand\Api\Data\BrandInterface;
//
//use Magento\Framework\Api\SearchCriteriaInterface;
//
//
//
///**
// * Class BrandRepository
// * @package Tanya\Brand\Model
// */
//class BrandRepository implements BrandRepositoryInterface
//{
//    /**
//     * @var CollectionFactory
//     */
//    private $collectionFactory;
//
//    /**
//     * BrandRepository constructor.
//     * @param CollectionFactory $collectionFactory
//     */
//    public function __construct(
//        CollectionFactory $collectionFactory
//    )
//    {
//        $this->collectionFactory = $collectionFactory;
//    }
//
//    /**
//     * @param $brand
//     * @return Resource\Brand\Collection
//     */
//    public function save(BrandInterface $brand)
//    {
//        $parameter = $this->collectionFactory->create();
//        $parameter->setData($brand);
//        return $parameter->save();
//    }
//
//    /**
//     * @param $id
//     * @return \Magento\Framework\DataObject
//     */
//    public function getById($id)
//    {
//        $parameter = $this->collectionFactory->create();
//        $brand = $parameter->getItemById($id);
//        return $brand;
//    }
//    public function getList()
//    {
//        $list = $this->collectionFactory->create();
//        $brands = $list->getItems();
//        return $brands;
//    }
//    /**
//     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
//     * @return \Tanya\Brand\Api\Data\BrandInterface
//     */
//
//    public function getList(SearchCriteriaInterface $searchCriteria)
//    {
//    }
//
//    /**
//     * @param $brand
//     * @return bool
//     */
//    public function delete(BrandInterface $brand)
//    {
//        try {
//            $this->brandFactory->delete($brand);
//        } catch (\Exception $exception) {
//            throw new CouldNotDeleteException(__($exception->getMessage()));
//        }
//        return true;
//    }
//
//    /**
//     * @param $id
//     * @return bool
//     */
//    public function deleteById($id)
//    {
//        return $this->delete($this->getById($id));
//    }
//}