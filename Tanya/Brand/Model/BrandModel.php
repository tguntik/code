<?php

namespace Tanya\Brand\Model;

use Magento\Framework\Model\AbstractModel;

use Tanya\Brand\Api\Data\BrandInterface;

/**
 * Class BrandModel
 * @package Tanya\Brand\Model
 */
class BrandModel extends AbstractModel implements BrandInterface
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Tanya\Brand\Model\Resource\Brand::class);
    }
    /**
     * @return int
     */
    public function getId()
    {
        return $this->getData('brand_id');
    }
    /**
     * @return string
     */
    public function getName()
    {
        return $this->getData('name');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getData('description');
    }
    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        return $this->setData('brand_id', $id);
    }
    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        return $this->setData('name', $name);
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        return $this->setData('description', $description);
    }
}
