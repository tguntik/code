<?php

namespace Tanya\Brand\Model\Resource\Brand;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
/**
 * Class Collection
 * @package Tanya\Brand\Model\Resource\Brand
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'brand_id';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Tanya\Brand\Model\BrandModel',
            'Tanya\Brand\Model\Resource\Brand'
        );
    }
}