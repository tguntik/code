<?php

namespace Tanya\Brand\Model\Resource;

use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Profile
 * @package Tanya\Brand\Model\Resource
 */
class Brand extends AbstractDb
{
    /**
     * Define main table and initialize connection
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('tanya_brand', 'brand_id');
    }
}
