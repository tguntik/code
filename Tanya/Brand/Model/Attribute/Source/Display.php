<?php

namespace Tanya\Brand\Model\Attribute\Source;

use Tanya\Brand\Model\BrandRepository;
use \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

/**
 * Class Display
 * @package Tanya\Brand\Model\Attribute\Source
 */
class Display extends AbstractSource
{
    /**
     * @var BrandRepository
     */
    protected $brandRepository;

    /**
     * Test constructor.
     * @param array $data
     */
    public function __construct(
        BrandRepository $brandRepository
    )
    {
        $this->brandRepository = $brandRepository;
    }

    /**
     * Get all options
     * @return array
     */
    public function getAllOptions()
    {
        $myBrands = $this->brandRepository->getList();

        if (!$this->_options) {
            $this->_options = [];

            foreach ($myBrands as $item) {
                $this->_options[] = ['label' => __($item->getName()), 'value' => 'dfsre'];
            }
        }
        return $this->_options;
    }
}