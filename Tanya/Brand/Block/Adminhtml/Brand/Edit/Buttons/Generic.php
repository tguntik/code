<?php

namespace Tanya\Brand\Block\Adminhtml\Brand\Edit\Buttons;

use Tanya\Brand\Model\BrandRepository;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Generic
 * @package Tanya\Brand\Block\Adminhtml\Brand\Edit\Buttons
 */
class Generic
{
    /**
     * @var Context
     */
    protected $context;
    /**
     * @var BrandRepository
     */

    protected $brandRepository;

    /**
     * Generic constructor.
     * @param Context $context
     * @param BrandRepository $brandRepository
     */

    public function __construct(
        Context $context,
        BrandRepository $brandRepository
    )
    {
        $this->context = $context;
        $this->brandRepository = $brandRepository;
    }

    /**
     * @return |null
     */
    public function getBrandId()
    {
        try {
            return $this->brandRepository->getById(
                $this->context->getRequest()->getParam('brand_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * @param string $route
     * @param array $params
     * @return string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
