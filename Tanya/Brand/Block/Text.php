<?php

namespace Tanya\Brand\Block;

use Magento\Framework\View\Element\Template\Context;

class Text extends \Magento\Framework\View\Element\Template
{

    /**
     * Text constructor.
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        array $data = [])
    {
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getText()
    {
        return "Text";
    }
}

