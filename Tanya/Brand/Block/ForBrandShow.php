<?php

namespace Tanya\Brand\Block;


use Tanya\Brand\Model\BrandRepository;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Api\SearchCriteriaBuilder;


/**
 * Class ForBrandShow
 * @package Tanya\Brand\Block
 */
class ForBrandShow extends Template
{
    /**
     * @var BrandRepository
     */
    protected $brandRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * Test constructor.
     * @param Context $context
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param array $data
     */
    public function __construct(
        Context $context,
        BrandRepository $brandRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        array $data = [])
    {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->brandRepository = $brandRepository;
        parent::__construct($context, $data);
    }

    /**
     * @return \Magento\Framework\DataObject[]
     */
//    /**
//     * @return \Tanya\Brand\Api\Data\BrandSearchResultInterface
//     */
    public function getList()
    {
        $searchCriteria = $this->searchCriteriaBuilder->create();
        return $this->brandRepository->getList($searchCriteria);
    }

}

