<?php

namespace Tanya\Brand\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Tanya\Brand\Helper\Email;
use  Magento\Checkout\Model\Session;

/**
 * Class MyObserver
 * @package Tanya\Brand\Observer
 */
class MyObserver implements ObserverInterface
{
    /**
     * @var Session
     */
    protected $session;
    /**
     * @var Email
     */

    protected $email;

    /**
     * MyObserver constructor.
     * @param Session $session
     * @param Email $email
     */
    public function __construct(
        Session $session,
        Email $email
    )
    {
        $this->email = $email;
        $this->session = $session;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $totalQty = $this->session->getQuote()->getItemsQty();
        if ($totalQty > 5){
            return $this->email->sendEmail();
        }
    }
}