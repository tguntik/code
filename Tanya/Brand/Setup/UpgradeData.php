<?php

namespace Tanya\Brand\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Class UpgradeData
 * @package Tanya\Brand\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if ($context->getVersion()
            && version_compare($context->getVersion(), '0.0.2') < 0
        ) {
            $table = $setup->getTable('tanya_brand');

            $setup->getConnection()
                ->update($table, ['name' => 'HHHNike', 'description' => 'Nike description'], 'brand_id IN (1)');

            $setup->getConnection()
                ->insertForce($table, ['name' => 'Adidas', 'description' => 'Adidas description']);

            $setup->getConnection()
                ->insertForce($table, ['name' => 'Puma', 'description' => 'Puma description']);

        }
        $setup->endSetup();
    }
}

//use Magento\Framework\Setup\ModuleContextInterface;
//use Magento\Framework\Setup\ModuleDataSetupInterface;
//use Magento\Framework\Setup\UpgradeDataInterface;
//
//use Magento\Eav\Setup\EavSetupFactory;
//use Magento\Cms\Model\PageFactory;
//use Magento\Cms\Api\PageRepositoryInterface;


///**
// * @codeCoverageIgnore
// */
//class UpgradeData implements UpgradeDataInterface
//{
//    /**
//     * @var \Magento\Cms\Model\PageFactory
//     */
//    protected $_pageFactory;
//
//    /**
//     * Construct
//     *
//     * @param \Magento\Cms\Model\PageFactory $pageFactory
//     */
//
//    public function __construct(
//        EavSetupFactory $eavSetupFactory,
//        PageRepositoryInterface $pageRepository,
//        PageFactory $pageFactory
//    )
//    {
//        $this->eavSetupFactory = $eavSetupFactory;
//        $this->pageRepository = $pageRepository;
//        $this->pageFactory = $pageFactory;
//    }
//
//    /**
//     * @param ModuleDataSetupInterface $setup
//     * @param ModuleContextInterface $context
//     */
//    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
//    {
//        $setup->startSetup();
//
//        if (version_compare($context->getVersion(), '1.1') < 0) {
//            $page = $this->_pageFactory->create();
//
//            $page = $this->_pageFactory->create();
//            $page->setTitle('Example CMS page')
//                ->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit.')
//                ->save();
//        }
//
//        $setup->endSetup();
//    }
//}
