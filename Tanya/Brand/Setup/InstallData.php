<?php

namespace Tanya\Brand\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

use Magento\Eav\Setup\EavSetupFactory;

/**
 * Class InstallData
 * @package Tanya\Brand\Setup
 */
class InstallData implements InstallDataInterface
{
    /**
     * Eav setup factory
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * Init
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetupFactory->create();
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,

            'brand_origin',
            [
                'group' => 'General',
                'type' => 'varchar',
                'label' => 'Brand origin',
                'input' => 'select',
                'source' => 'Tanya\Brand\Model\Attribute\Source\Display',
                'required' => false,
                'sort_order' => 50,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'visible' => true,
                'is_html_allowed_on_front' => true,
                'visible_on_front' => true
            ]
        );
    }

//    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
//    {
//        $data = [
//            ['name' => 'Nike', 'description' => 'Nike description'],
//
//        ];
//        foreach ($data as $bind) {
//            $setup->getConnection()
//                ->insertForce($setup->getTable('tanya_brand'), $bind);
//        }
//    }

}

