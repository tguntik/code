<?php

namespace Tanya\Brand\Plugin;

/**
 * Class ChangeCopyright
 * @package Tanya\Brand\Plugin
 */
class ChangeCopyright
{
    /**
     * @param \Magento\Theme\Block\Html\Footer $subject
     * @param $result
     * @return string
     */
    public function afterGetCopyright(
        \Magento\Theme\Block\Html\Footer $subject,
        $result
    )
    {
        return '@' . $result . '@';
    }
}
