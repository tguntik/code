<?php

namespace Tanya\Brand\Controller\Index;

use \Magento\Framework\App\Action\Action;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Index
 * @package Tanya\Brand\Controller\Index
 */
class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $_pageFactory;
    /**
     * @var \Tanya\Brand\Helper\Data
     */
    protected $helperData;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param \Tanya\Brand\Helper\Data $helperData
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        \Tanya\Brand\Helper\Data $helperData
        )
    {
        $this->_pageFactory = $pageFactory;
        $this->helperData = $helperData;
        return parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $brandConfig = $this->helperData->getGeneralConfig('enable');
        if ($brandConfig) {
            return $this->_pageFactory->create();
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('/');

    }
}






