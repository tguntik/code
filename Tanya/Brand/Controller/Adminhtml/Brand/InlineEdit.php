<?php

//namespace Tanya\Brand\Controller\Adminhtml\Brand;
//
//use Magento\Backend\App\Action;
//
//use Tanya\Brand\Model\Profile;
//use Tanya\Brand\Model\DistributorProfile;
//
//use Magento\Framework\Controller\Result\JsonFactory;
//
//class InlineEdit extends \Magento\Backend\App\Action
//{
//    private $jsonFactory;
//
//    public function __construct(
//        Action\Context $context,
//        JsonFactory $jsonFactory
//    )
//    {
//        $this->jsonFactory = $jsonFactory;
//        parent::__construct($context);
//    }
//
//    public function execute()
//    {
//        $this->_view->loadLayout();
//        $this->_view->renderLayout();
//        $profiles = $this->getRequest()->getParam('items');
//        if(is_array($profiles)) {
//            $type = $this->getRequest()->getParam('profile_type', 'profile');
//            switch ($type) {
//                case 'profile':
//                    $class = Profile::class;
//                    break;
//                case 'distributor':
//                    $class = DistributorProfile::class;
//                    break;
//            }
//            if (!empty($class)) {
//                foreach ($profiles as $profile) {
//                    $profileObject = $this->_objectManager->create($class);
//                    $profileObject->setData($profile)->save();
//                }
//            }
//            if ($this->getRequest()->getParam('isAjax', false)) {
//                $resultJson = $this->jsonFactory->create();
//                $resultJson->setData(
//                    [
//                        'messages' => [],
//                        'error' => false
//                    ]
//                );
//                return $resultJson;
//            }
//            $resultRedirect = $this->resultRedirectFactory->create();
//            return $resultRedirect->setPath('*/*/index');
//        }
//    }
//}