<?php

namespace Tanya\Callback\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Tanya\Callback\Helper\Email;

class SendCallbackMail implements ObserverInterface
{
    /**
     * @var Email
     */
    protected $email;

    /**
     * MyObserver constructor.
     * @param Email $email
     */
    public function __construct(
        Email $email
    )
    {
        $this->email = $email;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        return $this->email->sendEmail();
    }
}