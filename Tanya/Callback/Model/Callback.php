<?php

namespace Tanya\Callback\Model;

class Callback extends \Magento\Framework\Model\AbstractModel
{

    public function _construct()
    {
        $this->_init('Tanya\Callback\Model\ResourceModel\Callback');
    }
}