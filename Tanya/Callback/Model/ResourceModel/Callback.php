<?php

namespace Tanya\Callback\Model\ResourceModel;

class Callback extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    public function _construct()
    {
        $this->_init('tanya_callback', 'id');
    }
}