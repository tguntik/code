<?php

namespace Tanya\Callback\Model\ResourceModel\Callback;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Tanya\Callback\Model\Callback', 'Tanya\Callback\Model\ResourceModel\Callback');
    }
}
