<?php

namespace Tanya\Callback\Block\Html;


class CustomHeader extends \Magento\Theme\Block\Html\Header
{
    protected $template = 'Magento_Theme::html/header.phtml';
    protected $customerSession;

    public function __construct(\Magento\Customer\Model\Session $customerSession, \Magento\Framework\View\Element\Template\Context $context, array $data = [])
    {
        $this->customerSession = $customerSession;

        parent::__construct($context, $data);
    }


    /**
     * Logic For Welcome Text
     *
     * @return string
     */
    public function getWelcome()
    {
        if ($this->customerSession->isLoggedIn()) {
            return parent::getWelcome();
        } else {
            return __('Hi Guest!');
        }
    }
}