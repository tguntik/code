<?php

namespace Tanya\Callback\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Escaper;
use Magento\Framework\Mail\Template\TransportBuilder;

//use Tanya\Callback\Helper\GetDataForMail;

use Tanya\Callback\Model\CallbackFactory;

/**
 * Class Email
 * @package Tanya\Callback\Helper
 */
class Email extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $callback;
    /**
     * @var StateInterface
     */
    protected $inlineTranslation;
    /**
     * @var Escaper
     */
    protected $escaper;
    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
//    /**
//     * @var \Tanya\Callback\Helper\GetDataForMail
//     */
//    protected $dataMail;


    public function __construct(
        Context $context,
        StateInterface $inlineTranslation,
        Escaper $escaper,
        TransportBuilder $transportBuilder,

        CallbackFactory $callback
//        GetDataForMail $getDataForMail
    ) {
        parent::__construct($context);
        $this->inlineTranslation = $inlineTranslation;
        $this->escaper = $escaper;
        $this->transportBuilder = $transportBuilder;
        $this->logger = $context->getLogger();

        $this->callback = $callback;

//        $this->dataMail = $getDataForMail;
    }

    /**
     * send mail
     */
    public function sendEmail()
    {
//        $sender = $this->dataMail->getGeneralConfig('sender_email');
//        $receiver = $this->dataMail->getGeneralConfig('receiver_email');

        $factory = $this->callback->create();
        $collection = $factory->getCollection();
        $data = $collection->getData();

        $numberOfLastUser = count($data)-1;

        $userName = $data[$numberOfLastUser]['name'];
        $userPhone = $data[$numberOfLastUser]['phone'];
        $userComment = $data[$numberOfLastUser]['comment'];


        try {
            $this->inlineTranslation->suspend();
            $sender = [
                'name' => $this->escaper->escapeHtml('Test'),
                'email' => $this->escaper->escapeHtml('Bender2@example.com'),
            ];
            $transport = $this->transportBuilder
                ->setTemplateIdentifier('email_callback_template')
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars([
                    'name'  => $userName,
                    'phone' => $userPhone,
                    'comment' => $userComment,
                ])
                ->setFrom($sender)
                ->addTo('Receiver2@example.com')
                ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
        }
    }
}