<?php

namespace Tanya\Callback\Controller\Ajax;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

use Tanya\Callback\Model\CallbackFactory;

class Index extends Action
{
    protected $callback;

    public function __construct(Context $context, CallbackFactory $callback)
    {
        $this->callback = $callback;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $callback = $this->callback->create();
        $callback->setData($data);
        $callback->save();

        $nameobj = new \Magento\Framework\DataObject(array('name' => 'Emailcallback'));
        $this->_eventManager->dispatch('send_email_callback_form', ['obj' => $nameobj]);

        $response = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $response->setData($callback->getData());
        return $response;
    }
}