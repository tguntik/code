<?php

namespace Tanya\GraphQl\Model\Resolver;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

use Tanya\GraphQl\Model\Resolver\Brand\BrandDataProvider;

/**
 * Class Brand
 * @package Tanya\GraphQl\Model\Resolver
 */
class Brand implements ResolverInterface
{
    /**
     * @var BrandDataProvider
     */
    private $brandDataProvider;

    /**
     * Brand constructor.
     * @param BrandDataProvider $brandDataProvider
     */
    public function __construct(
        BrandDataProvider $brandDataProvider
    ) {
        $this->brandDataProvider = $brandDataProvider;
    }

    /**
     * @param Field $field
     * @param \Magento\Framework\GraphQl\Query\Resolver\ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return array|\Magento\Framework\GraphQl\Query\Resolver\Value|mixed
     * @throws GraphQlNoSuchEntityException
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        return $brandData = $this->brandDataProvider->getBrand();
    }
}