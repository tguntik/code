<?php

namespace Tanya\GraphQl\Model\Resolver\Brand;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Tanya\Brand\Model\BrandModelFactory;

/**
 * Class BrandDataProvider
 * @package Tanya\GraphQl\Model\Resolver\Brand
 */
class BrandDataProvider
{
    /**
     * @var BrandModelFactory
     */
    protected $brandFactory;
    /**
     * @var \Tanya\Brand\Model\BrandRepository
     */
    protected $brandRepository;

    /**
     * BrandDataProvider constructor.
     * @param BrandModelFactory $brandFactory
     * @param \Tanya\Brand\Model\BrandRepository $brandRepository
     */
    public function __construct(
        BrandModelFactory $brandFactory,
    \Tanya\Brand\Model\BrandRepository $brandRepository
    )
    {
        $this->brandFactory  = $brandFactory;
        $this->brandRepository = $brandRepository;
    }

    /**
     * @return array
     * @throws GraphQlNoSuchEntityException
     */
    public function getBrand( )
    {
        try {
            $collection = $this->brandFactory->create()->getCollection();
            $brandData = $collection->getData();

        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }
        return $brandData;
    }

    /**
     * @param $name
     * @param $description
     * @return mixed
     * @throws GraphQlNoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function brandCreate($name, $description)
    {
        try {
            $model = $this->brandFactory->create();
            $model->setData('name', $name);
            $model->setData('description', $description);
            $this->brandRepository->save($model);
        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }
        $thanks_message['success_message']="Thanks For Creating New Brand";
        return $thanks_message;
    }
}