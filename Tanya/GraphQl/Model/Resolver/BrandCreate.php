<?php

namespace Tanya\GraphQl\Model\Resolver;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

/**
 * Class BrandCreate
 * @package Tanya\GraphQl\Model\Resolver
 */
class BrandCreate implements ResolverInterface
{
    /**
     * @var Brand\BrandDataProvider
     */
    private $brandDataProvider;

    /**
     * BrandCreate constructor.
     * @param Brand\BrandDataProvider $brandDataProvider
     */
    public function __construct(
        \Tanya\GraphQl\Model\Resolver\Brand\BrandDataProvider $brandDataProvider
    ) {
        $this->brandDataProvider = $brandDataProvider;
    }

    /**
     * @param Field $field
     * @param \Magento\Framework\GraphQl\Query\Resolver\ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return \Magento\Framework\GraphQl\Query\Resolver\Value|mixed
     * @throws GraphQlNoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $name = $args['input']['name'];
        $description = $args['input']['description'];
        $success_message = $this->brandDataProvider->brandCreate(
            $name,
            $description
        );
        return $success_message;
    }
}