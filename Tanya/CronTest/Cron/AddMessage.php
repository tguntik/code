<?php


namespace Tanya\CronTest\Cron;


class AddMessage
{
    protected $logger;

    public function __construct(\Psr\Log\LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function addInfo()
    {
        $this->logger->info(__('This message is added by magenest_cron_add_info_log job'));
    }

    public function addDebug()
    {
        $this->logger->debug(__('This message is added by magenest_cron_add_debug_log job'));
    }
}