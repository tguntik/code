<?php
namespace Tanya\AddAdditionalShippingField\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class SaveOrderObserver implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $quote = $observer->getEvent()->getQuote();

        $fr = $quote->getCustomNotes();

        $f = $quote->getAttributes();

        $order->setCustomNotes($quote->getCustomNotes());

        $order->setData('custom_notes', $quote->getCustomNotes());

//        $order->setData('custom_notes', 'dffvfsdv');

        return $this;
    }
}