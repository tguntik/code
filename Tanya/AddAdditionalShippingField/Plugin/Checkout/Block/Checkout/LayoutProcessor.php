<?php
namespace Tanya\AddAdditionalShippingField\Plugin\Checkout\Block\Checkout;

use Magento\Checkout\Model\Session as CheckoutSession;

class LayoutProcessor
{
    public $checkoutSession;

    public function __construct(
        CheckoutSession $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
    }

    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array  $jsLayout
    ) {

        $products=$this->checkoutSession->getQuote()->getAllItems();
        $type = $products[0]->getProductType();
//        if($type == 'giftcard_product_type') {


        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['shipping-address-fieldset']['children']['custom_notes'] = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input',
                'options' => [],
                'id' => 'custom-notes'
            ],
            'dataScope' => 'shippingAddress.custom_attributes.custom_notes',
            'label' => 'Custom Notes',
            'provider' => 'checkoutProvider',
            'visible' => true,
            'validation' => [],
            'sortOrder' => 299,
            'id' => 'custom-notes'
        ];


        return $jsLayout;
    }
}