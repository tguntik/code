<?php
namespace Tanya\AddAdditionalShippingField\Block\Adminhtml\Order\View;

class OrderComment extends \Magento\Backend\Block\Template
{
  protected $_registry;

  public function __construct(
    \Magento\Backend\Block\Template\Context $context,
    \Magento\Framework\Registry $registry,
    array $data = []
  ) {
      $this->_registry = $registry;
      parent::__construct($context, $data);
  }

  public function getOrderComment() {
    if($currentOrder = $this->_registry->registry('current_order')) {
//      return $currentOrder->getHolusormiOrdercomment();
        return $currentOrder->getCustomNotes();
    }
  }
}
