var config = {
    "map": {
        "*": {
            'Magento_Checkout/js/model/shipping-save-processor/default': 'Tanya_AddAdditionalShippingField/js/model/shipping-save-processor/default'
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/action/set-shipping-information': {
                'Tanya_AddAdditionalShippingField/js/action/set-shipping-information-mixin': true
            }
        }
    },
};