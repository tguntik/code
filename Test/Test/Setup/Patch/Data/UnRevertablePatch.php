<?php

/**
 * Created by PhpStorm.
 * User: tatyana
 * Date: 2020-10-30
 * Time: 14:35
 */

namespace Test\Test\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\SchemaPatchInterface;

use Magento\Framework\Setup\ModuleDataSetupInterface;

class UnRevertablePatch implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface $moduleDataSetup
     */
    private $moduleDataSetup;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(ModuleDataSetupInterface $moduleDataSetup)
    {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * Do Upgrade
     *
     * @return void
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();


        $setup = $this->moduleDataSetup;


        $data[] = ['name' => 'UnRevertable name', 'description' => 'UnRevertable description text'];

        $data[] = ['name' => 'UnRevertable name', 'description' => 'UnRevertable description text'];


        $this->moduleDataSetup->getConnection()->insertArray(

            $this->moduleDataSetup->getTable('test_test'),

            ['test_name', 'test_description'],

            $data

        );

        $this->moduleDataSetup->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [

        ];
    }
}
